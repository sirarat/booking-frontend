import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path: '/Calendar',
    name: 'Calendar',
    component: () => import(/* webpackChunkName: "about" */ '../views/Calendar.vue')
  },
  {
    path: '/UserReservation',
    name: 'UserReservation',
    component: () => import(/* webpackChunkName: "about" */ '../views/UserReservation.vue')
  },
  {
    path: '/ApproverReservation',
    name: 'ApproverReservation',
    component: () => import(/* webpackChunkName: "about" */ '../views/ApproverReservation.vue')
  },
  {
    path: '/ReservationDetail/:id',
    name: 'ReservationDetail',
    component: () => import(/* webpackChunkName: "about" */ '../views/ReservationDetail.vue')
  },
  {
    path: '/Approver',
    name: 'Approver',
    component: () => import(/* webpackChunkName: "about" */ '../views/Approver.vue')
  },
  {
    path: '/Agency',
    name: 'Agency',
    component: () => import(/* webpackChunkName: "about" */ '../views/Agency.vue')
  },
  {
    path: '/User',
    name: 'User',
    component: () => import(/* webpackChunkName: "about" */ '../views/User.vue')
  },
  {
    path: '/AgencyUser',
    name: 'AgencyUser',
    component: () => import(/* webpackChunkName: "about" */ '../views/AgencyUser.vue')
  },
  {
    path: '/Building',
    name: 'Building',
    component: () => import(/* webpackChunkName: "about" */ '../views/Building.vue')
  },
  {
    path: '/Room',
    name: 'Room',
    component: () => import(/* webpackChunkName: "about" */ '../views/Room.vue')
  },
  {
    path: '/HomePageNotLogin',
    name: 'HomePageNotLogin',
    component: () => import(/* webpackChunkName: "about" */ '../components/HomePageNotLogin.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
