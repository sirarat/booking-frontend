import { AUTH_LOGIN, AUTH_LOGOUT } from '../mutation-types'
import router from '../../router'
import { login } from '../../service/auth'
// import api from '../../service/api'
import { getUser } from '../../service/user'
// import axios from 'axios'
export default {
  namespaced: true,
  state: () => ({
    user: localStorage.getItem('user')
      ? JSON.parse(localStorage.getItem('user')) : null
  }),
  mutations: {
    [AUTH_LOGIN] (state, payload) {
      state.user = payload
    },
    [AUTH_LOGOUT] (state) {
      state.user = null
    }
  },
  actions: {
    async login ({ commit }, payload) {
      // const user = payload
      console.log(`payload email = ${payload.email}`)
      try {
        const res = await login(payload.email, payload.password)
        console.log(`res = ${res.data}`)
        const user = res.data.user
        const token = res.data.token
        localStorage.setItem('token', token)
        localStorage.setItem('user', JSON.stringify(user))
        const oneUser = await getUser(res.data.user._id)
        localStorage.setItem('firstname', JSON.parse(JSON.stringify(oneUser.data.firstname)))
        localStorage.setItem('lastname', JSON.parse(JSON.stringify(oneUser.data.lastname)))
        localStorage.setItem('status', JSON.stringify(oneUser.data.status))
        const test = JSON.parse(localStorage.getItem('status'))
        console.log(test)
        console.log(res)
        if (test.includes('แอดมินระบบ') || test.includes('แอดมินหน่วยงาน')) {
          router.push('/HomePageNotLogin')
        } else {
          router.push('/')
        }
        if (localStorage.getItem('user') !== null) {
          commit(AUTH_LOGIN, user)
        }
        // commit(AUTH_LOGIN, user)
      } catch (e) {
        console.log('Error')
      }
    },
    logout ({ commit }) {
      localStorage.removeItem('token')
      localStorage.removeItem('user')
      localStorage.removeItem('firstname')
      localStorage.removeItem('lastname')
      localStorage.removeItem('status')
      localStorage.removeItem('building')
      commit(AUTH_LOGOUT)
    }
  },
  getters: {
    isLogin (state, getters) {
      return state.user != null
    },
    getStatus () {
      return localStorage.getItem('status')
    }
  }
}
