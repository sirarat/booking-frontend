import api from './api'

export function getUsers () {
  return api.get('/users')
}
export function getUser (x) {
  return api.get('/users/' + x)
}
export function getUserByAgency (x) {
  return api.get('/users/a/' + x)
}
export function getUserByAgencyID (x) {
  return api.get('/users/b/' + x)
}
export function updateAdminAgency (x) {
  return api.patch('/users/' + x)
}
export function addUser (x) {
  return api.post('/users', x)
}
export function updateUser (i, c) {
  return api.put('/users/' + i, c)
}
export function delUser (x) {
  return api.delete('/users/' + x)
}
