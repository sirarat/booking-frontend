import api from './api'

export function getRooms () {
  return api.get('/rooms')
}
export function getRoom (x) {
  return api.get('/rooms/' + x)
}
export function getRoomsByAgency (x) {
  return api.get('/rooms/a/' + x)
}
export function addRoom (x) {
  return api.post('/rooms', x)
}
export function updateRoom (id, x) {
  return api.put('/rooms/' + id, x)
}
export function delRoom (x) {
  return api.delete('/rooms/' + x)
}
