import api from './api'

export function getAgencies () {
  return api.get('/agencys')
}
export function addAgency (agency) {
  return api.post('/agencys', agency)
}
export function getAgency (id) {
  return api.get('/agencys/' + id)
}
export function deleteAgency (agency) {
  return api.delete('/agencys/' + agency)
}
export function updateAgency (agency, id) {
  return api.put('./agencys/' + id, agency)
}
