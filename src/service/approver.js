import api from './api'

export function getApprovers (x) {
  return api.get('/approvers/a/' + x)
}

export function addApprover (x) {
  return api.post('/approvers', x)
}

export function updateApprover (x, approver) {
  return api.put('/approvers/' + x, approver)
}

export function getApprover (x) {
  return api.get('/approvers/' + x)
}

export function deleteApprover (x) {
  return api.delete('/approvers/' + x)
}
