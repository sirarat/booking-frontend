import api from './api'

export function getEvents () {
  return api.get('/bookingforms')
}
export function getEvent (x) {
  return api.get('/bookingforms/detail/' + x)
}
export function getBookingFormsByRoomID (x) {
  return api.get('/bookingforms/room/' + x)
}

export function addBookingForm (event) {
  return api.post('/bookingforms', event)
}
export function getReservation (id) {
  return api.get('/bookingforms/' + id)
}
export function confirmReservation (id) {
  return api.post('/bookingforms/confirm/' + id)
}
export function noconfirmReservation (id) {
  return api.post('/bookingforms/cancel/' + id)
}
export function getBookingFormsByUserId (x) {
  return api.get('/bookingforms/user/' + x)
}
export function deleteBookingForm (id) {
  return api.delete('/bookingforms/' + id)
}
