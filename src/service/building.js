import api from './api'

export function getBuildings () {
  return api.get('/buildings')
}
export function getBuilding (x) {
  return api.get('/buildings/' + x)
}
export function addBuilding (x) {
  return api.post('/buildings', x)
}
export function updateBuilding (x, id) {
  return api.patch('/buildings/' + id, x)
}
export function delBuilding (x) {
  return api.delete('/buildings/' + x)
}
